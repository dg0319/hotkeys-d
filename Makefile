CC=gcc
CFLAGS=-Wall -g -std=c99
LIBS=-lyaml -pthread
DEFINES=-D _GNU_SOURCE # -D DEBUG_MAIN -D DEBUG_CONFIG

hotkeys-d: keydev.h tty_util.h inotify_util.h util.h log.h keycode.h config.h keydev.c config.c keycode.c log.c util.c inotify_util.c tty_util.c main.c 
	$(CC) $(CFLAGS) $(LIBS) $(DEFINES) $^ -o $@

