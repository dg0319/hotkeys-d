#include <termios.h>
#include <string.h>
#include "tty_util.h"

static struct termios original_term;

void disable_echo(void)
{
	tcgetattr(0, &original_term);
	struct termios echoless_term;
	memcpy(&echoless_term, &original_term, sizeof(struct termios));
	echoless_term.c_lflag &= (~ECHO);
	tcsetattr(0, TCSANOW, &echoless_term);
}

void enable_echo(void)
{
	tcsetattr(0, TCSANOW, &original_term);
}

