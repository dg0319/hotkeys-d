#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <err.h>
#include <string.h>
#include <ctype.h>
#include "keycode.h"
#include "util.h"
#include "log.h"

#ifdef DEBUG_KEYCODE
	#define DK_PRINTF printf
	#define DK_PUTS puts
	#define DK_PUTSS printf("[Keycodes] ");puts
	#define DK_PUTCHAR putchar
	#define DK_PRINTFS printf("[Keycodes] ");printf
#else
	#define DK_PRINTF(...)
	#define DK_PUTS(...)
	#define DK_PUTCHAR(...)
	#define DK_PUTSS(...)
	#define DK_PRINTFS(...)
#endif

/* Get newline count in a char *str */
static int get_line_count(char *str, long sz)
{
	int count = 0;
	for (size_t i = 0; i < sz; i++)
		if (str[i] == '\n')
			count++;
	return count;
}

/* Returns true if the char *value is written in 0xFF-like HEX format */
static bool is_hex(char *value, int valuelen)
{
	if (*(value + 1) == 'x' && value[0] == '0') {
		for (size_t i = 2; i < valuelen; i++) {
			if (!isxdigit( *(value + i) )) {
				DK_PRINTFS("'%s' is not a hex number!\n", value);
				return false;
			}
		}
	} else {
		DK_PRINTFS("'%s' is not a hex number!\n", value);
		return false;
	}
	return true;
}

/* Returns true if char *value is a decimal number */
static bool is_number(char *value, int valuelen)
{
	for (size_t i = 0; i < valuelen; i++) {
		if (!isdigit( *(value + i) )) {
			DK_PRINTFS("'%s' is not a decimal number!\n", value);
			return false;
		}
	}
	return true;
}

struct keycode_t *get_keycode(char *key_name)
{
	for (size_t i = 0; i < keycode_count; i++) {
		if (strcmp(key_name, keycodes[i].name) == 0) {
			DK_PRINTFS("Found keycode alias '%s', its value is %i\n",
				       	key_name, keycodes[i].value);
			return &(keycodes[i]);
		}
	}

	/* Check for being a value of a keycode */
	int dec_value = -1;
	if (is_hex(key_name, strlen(key_name)))
		dec_value = strtol(key_name, 0, 16);
	else if (is_number(key_name, strlen(key_name)))
		dec_value = strtol(key_name, 0, 10);
	else
		return 0;

	/* Find keycode_t with this value */
	for (size_t i = 0; i < keycode_count; i++) {
		if (dec_value == keycodes[i].value) {
			DK_PRINTFS("Found keycode value '%s', its name is '%s'\n",
				       	key_name, keycodes[i].name);
			return &(keycodes[i]);
		}
	}

	return 0;
}

void parse_keycodes(char *data, long datasz)
{
	int line_count = get_line_count(data, datasz);
	DK_PRINTFS("Total line count: %i\n", line_count);

	/* Keycode_t array used to store keycode name-value pairs */
	keycodes = (struct keycode_t*)calloc(line_count, sizeof(struct keycode_t));

	/* Parse every line as a name-value pair */
	char *save;
	char *token = strtok_r(data, "\n", &save);
	size_t parsed_count = 0;
	while (token != 0) {
		DK_PRINTFS("Line %li: %s\n", parsed_count, token);

		/* Search for the delimiter between name and value ('\t') */
		char *delimptr = strchr(token, '\t');
		if (delimptr == 0) {
			DK_PRINTFS("Line %li: Delimiter not found,\
				       	this line won't be parsed\n", parsed_count);
			goto skip;
		}

		/* Get name, truncate it if it is too long, and copy it to namebuf */
		char namebuf[KEY_NAME_MAX];
		if (delimptr - token >= KEY_NAME_MAX) {
			DK_PRINTFS("Line %li: Name is too long (%li symbols),\
				       	it will be truncated to %i\n",
					parsed_count, (delimptr - token), KEY_NAME_MAX - 1);
			strncpy(namebuf, token, KEY_NAME_MAX - 1);
			namebuf[KEY_NAME_MAX - 1] = '\0';
		} else {
			strncpy(namebuf, token, delimptr - token);
			namebuf[delimptr - token] = '\0';
		}
		char base;
		char *valueptr = delimptr + 1;
		size_t valuelen = strlen(valueptr);

		/* Check for the value's base */
		char valuebuf[KEY_VALUE_MAX];
		int dec_value; 
		/* Integer value of a valuebuf, final value to be stored in struct keycode_t */
		if (valuelen < 1) {
			DK_PUTS("No value is present after delimiter, skipping this token");
			goto skip;
		} else {
			/* Determine the base of a value */
			if (is_hex(valueptr, valuelen)) /* Check if it is a HEX number */
			{
				base = 16;
			}
			else {
				/* Check if it is an alias or a decimal number */
				if (!is_number(valueptr, valuelen)) {
					DK_PUTSS("Not a decimal, checking for beign an alias");
					/* Check if it is an alias (try to find any) */
					struct keycode_t *aliasptr = get_keycode(valueptr);
					if (aliasptr == 0) {
						log_msg(LOG_MSG_WARNING, "Invalid value '%s' for key '%s', it won't be parsed\n", 
								valueptr, namebuf);
						goto skip;
					} else {
						dec_value = aliasptr->value;
						goto alias_copy;
					}
				} else 
					base = 10;
			}
		}
		DK_PRINTFS("Value base: %i\n", base);
		/* Copy value to valuebuf, truncate if too long */
		if (valuelen >= KEY_VALUE_MAX) {
			DK_PRINTFS("Line %li: Value is too long (%li symbols), it will be truncated to %i\n",
					parsed_count, (delimptr - token), KEY_VALUE_MAX - 1);
			strncpy(valuebuf, valueptr, KEY_VALUE_MAX - 1);
			valuebuf[KEY_VALUE_MAX - 1] = '\0';
		} else {
			strncpy(valuebuf, valueptr, valuelen);
			valuebuf[valuelen] = '\0';
		}
		/* Parse valuebuf as an integer */
		dec_value = strtol(valuebuf, 0, base);
		alias_copy: ;
		/* Add newly created keycode_t struct to a keycodes array */
		char *nameptr = keycodes[parsed_count].name;
		strcpy(nameptr, namebuf);
		keycodes[parsed_count].value = dec_value;
		parsed_count++;
		keycode_count = parsed_count;
		skip:
		token = strtok_r(0, "\n", &save);
	}
	if (keycode_count < 1)
		exit_err("No pairs found in keycodes file (%s)", EXIT_WITHOUT_ERRNO);
}

void load_keycodes(void)
{
	long fsz = 0;
	char *fdata;
	fread_to_array(keycode_path, &fdata, &fsz);
	parse_keycodes(fdata, fsz);
	free(fdata);
	DK_PRINTFS("Parsed total %li keycode entries\n", keycode_count);
}

