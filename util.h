#ifndef UTIL_H
#define UTIL_H

#include <stdarg.h>
#include <stdbool.h>

#define EXIT_WITH_ERRNO true
#define EXIT_WITHOUT_ERRNO false

extern bool detached;

/* Outputs *str message, errno message(if print_errno is true) to the stderr and syslog,
 * then exits the program */
void exit_err(char *str, bool print_errno, ...);

void daemonize(void);

/* Reads file from *path, allocated array (*data) for its contents,
 * copies its contents into *data, sets *dsz to the length of *data */
void fread_to_array(const char* path, char **data, long *data_len);

#endif

