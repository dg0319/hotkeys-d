#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <err.h>
#include <error.h>
#include <yaml.h>
#include <stdarg.h>
#include "keycode.h"
#include "util.h"
#include "config.h"

#ifdef DEBUG_CONFIG
	#define DC_PRINTF printf
	#define DC_PUTS puts
	#define DC_PUTSS printf("[Config parser] ");puts
	#define DC_PUTCHAR putchar
	#define DC_PRINTFS printf("[Config parser] ");printf
#else
	#define DC_PRINTF(...)
	#define DC_PUTS(...)
	#define DC_PUTSS(...) 
	#define DC_PUTCHAR(...)
	#define DC_PRINTFS(...)
#endif

static const char stage_name[] = "[Config]";

static enum ParserState {
	DOCUMENT, /* Parsing root of the document */
	BINDING_INIT, /* Starting to parse the binding */
	KEYS_START, /* Staring to parse the key list */
	KEYS_VALUE, /* Key list parsing is in process */
	KEYS_END, /* Key list was parsed */
	ACTION_KEY,
	ACTION_VALUE,
	AUTOREPEAT_KEY,
	AUTOREPEAT_VALUE
} parser_state = DOCUMENT;

enum parser_err_type {
	MISSING_FIELD,
	UNKNOWN_FIELD,
	BROKEN_STRUCT,
	EMPTY_DOCUMENT,
	UNKNOWN_KEYCODE,
	EMPTY_KEY_LIST
};

/* Convert YAML boolean value (in a form of a string) to bool */
static inline bool value_to_bool(char * str)
{
	return ((strcasecmp(str, "true") == 0) || (strcasecmp(str, "yes") == 0) ||
		       	(strcasecmp(str, "y") == 0));
}

/* Execute actions based on the error */
static void handle_error(enum parser_err_type err_type, ...)
{
	/* Initialize varargs */
	va_list args;
	va_start(args, err_type);
	switch (err_type) {
	case MISSING_FIELD: {
		/* Varargs needed:
		   char *binding_name, char *field_name */
		char *binding_name = va_arg(args, char *);
		char *field_name = va_arg(args, char *);
		exit_err("%s Missing field '%s' in binding '%s'\n", 
				EXIT_WITHOUT_ERRNO, stage_name, field_name, binding_name);
		}
		break;
	case UNKNOWN_FIELD: {
		/* Varargs needed:
		   char *binding_name, char *field_name */
		char *binding_name = va_arg(args, char *);
		char *field_name = va_arg(args, char *);
		exit_err("%s Unknown field '%s ' in binding '%s '\n", 
				EXIT_WITHOUT_ERRNO, stage_name, field_name, binding_name);
		}
		break;
	case BROKEN_STRUCT: {
		char *binding_name = va_arg(args, char *);
		exit_err("%s Possibly broken YAML structure in binding '%s '\n", 
				EXIT_WITHOUT_ERRNO, stage_name, binding_name);
		}
		break; 
	case EMPTY_DOCUMENT:
		exit_err("%s File is empty\n", EXIT_WITHOUT_ERRNO, stage_name);
		break;
	case UNKNOWN_KEYCODE: {
		char *binding_name = va_arg(args, char *);
		char *keycode = va_arg(args, char *);
		exit_err("%s Unknown keycode '%s ' in binding '%s '\n", EXIT_WITHOUT_ERRNO,
			       	stage_name, keycode, binding_name);
		}
		break; 
	case EMPTY_KEY_LIST: {
		char *binding_name = va_arg(args, char *);
		exit_err("%s Empty key list in binding '%s '\n", EXIT_WITHOUT_ERRNO, 
				stage_name, binding_name);
		}
		break; 
	}
	va_end(args);		
}

/* Copy string from *from to **to, where **to is unallocated *char string */
static void alloc_strcpy(char **to, char *from)
{
	size_t from_len = strlen(from);
	*to = (char*)calloc(from_len + 1, sizeof(char));
	strcpy(*to, from);
}

void load_config(char *path)
{
	/* Open file for parsing */
	FILE *f;
	if (!(f = fopen(path, "r")))
		exit_err("%s Failed to open %s\n", EXIT_WITH_ERRNO, stage_name, path);

	/* Initialize parser */
	yaml_parser_t parser;
	if (!(yaml_parser_initialize(&parser)))
		exit_err("Failed to initialize YAML parser\n", EXIT_WITHOUT_ERRNO);

	/* Pass file to the YAML parser */
	yaml_parser_set_input_file(&parser, f); 

	/* Go through the document and get every token */
	yaml_token_t token;
	yaml_token_type_t last_token_type = YAML_NO_TOKEN;
	bindings = (struct binding_t*)calloc(1, sizeof(struct binding_t));
	binding_count = 0;
	bool parsed_keys = false;
	bool parsed_action = false;
	
	DC_PUTS("* Parsing YAML config *");
	struct binding_t *current_binding = 0;
	do {
		yaml_parser_scan(&parser, &token);
		DC_PRINTF("New token: %i, ", token.type);
		switch (token.type) {
		case YAML_STREAM_END_TOKEN:
			DC_PUTS("YAML_STREAM_END_TOKEN");
			if (last_token_type == YAML_STREAM_START_TOKEN)
				handle_error(EMPTY_DOCUMENT);
			break;
		case YAML_BLOCK_END_TOKEN:
			DC_PUTS("YAML_BLOCK_END_TOKEN ");
			if (parsed_keys && parsed_action && (last_token_type != YAML_BLOCK_END_TOKEN)) {
				DC_PUTS("Binding parse completed!");
				DC_PRINTF("\tname: '%s'\n", current_binding->name);
				DC_PRINTF("\tkeycount: %li\n", current_binding->keycount);
				DC_PRINTF("\tkeys: ");
				for (size_t i = 0; i < current_binding->keycount; i++)
					printf("%i ", current_binding->keys[i]);
				DC_PUTCHAR('\n');
				DC_PRINTF("\taction = '%s'\n", current_binding->action);
				DC_PRINTF("\thas_autorepeat = %i\n", current_binding->autorepeat);
				parser_state = DOCUMENT;
			} else if (!(parsed_keys && parsed_action) && (parser_state == BINDING_INIT)) {
				DC_PUTS("Broken (possibly) structure, encountered BLOCK_END without retrieving any required fields");
				handle_error(BROKEN_STRUCT, current_binding->name);
			} else if (!(parsed_keys && parsed_action) && (last_token_type == YAML_SCALAR_TOKEN || last_token_type == YAML_BLOCK_END_TOKEN)) {
				DC_PRINTF("Failed to parse binding '%s'\n", current_binding->name);
				if (!parsed_keys)
					handle_error(MISSING_FIELD, current_binding->name, "keys");
				if (!parsed_action)
					handle_error(MISSING_FIELD, current_binding->name, "action");
			}
			break;
		case YAML_FLOW_SEQUENCE_END_TOKEN:
			DC_PUTS("YAML_FLOW_SEQUENCE_END_TOKEN");
			if (parser_state == KEYS_VALUE) {
				DC_PUTS("Stopping keys array parsing");
				if (last_token_type == YAML_FLOW_SEQUENCE_START_TOKEN)
					handle_error(EMPTY_KEY_LIST, current_binding->name);
				parser_state = KEYS_END;
				parsed_keys = true;
			}
			break;
		case YAML_SCALAR_TOKEN:
			DC_PUTS("YAML_SCALAR_TOKEN!");
			DC_PRINTF("value: '%s'\n", token.data.scalar.value);
			if (parser_state == DOCUMENT && last_token_type == YAML_KEY_TOKEN) {
				DC_PRINTF("Creating a new binding with name '%s'\n", token.data.scalar.value);
				/* Create new binding_t struct */
				binding_count++;
				DC_PRINTF("Total binding count: %li\n", binding_count);
				bindings = reallocarray(bindings, binding_count, 
						sizeof(struct binding_t));
				/* Set current_binding to newly created struct */
				current_binding = &(bindings[binding_count - 1]);
				/* Set new binding's name */
				alloc_strcpy(&current_binding->name, (char *)token.data.scalar.value);
				parser_state = BINDING_INIT;
				current_binding->keycount = 0;
				current_binding->keys = 0;
				current_binding->autorepeat = false;
				/* Reset parsed_keys and parsed_action */
				parsed_keys = false;
				parsed_action = false;
			} else if (last_token_type == YAML_KEY_TOKEN) {
				if (strcmp((char *)token.data.scalar.value, "keys") == 0) {
					DC_PRINTF("Parsing key list for '%s'\n", current_binding->name);
					parser_state = KEYS_START;
				} else if (strcmp((char *)token.data.scalar.value, "action") == 0) {
					DC_PRINTF("Parsing action for '%s'\n", current_binding->name);
					parser_state = ACTION_KEY;
				} else if (strcmp((char *)token.data.scalar.value, "autorepeat") == 0) {
					DC_PUTS("Encountered autorepeat key");
					parser_state = AUTOREPEAT_KEY;
				} else { 
					/* Unknown field encountered, will stop parsing */
					DC_PUTS("Encountered unknown field");
					handle_error(UNKNOWN_FIELD, current_binding->name, token.data.scalar.value);
				} 
			} else if (parser_state == KEYS_START && last_token_type == YAML_FLOW_SEQUENCE_START_TOKEN) {
				DC_PRINTF("Parsing value from a key list: '%s'\n", token.data.scalar.value);
				/* Convert string name of a keycode to the numeric decimal value */
				struct keycode_t *keycode = get_keycode((char *)token.data.scalar.value);
				if (keycode == 0)
					handle_error(UNKNOWN_KEYCODE, current_binding->name, (char *)token.data.scalar.value);
				uint16_t dec_value = keycode->value;
				/* Add it to the binding->keys array */
				current_binding->keycount++;
				current_binding->keys = reallocarray(current_binding->keys, current_binding->keycount, sizeof(uint16_t));
				current_binding->keys[current_binding->keycount - 1] = dec_value;	
				parser_state = KEYS_VALUE;
			} else if (parser_state == KEYS_VALUE && (last_token_type == YAML_FLOW_SEQUENCE_START_TOKEN || last_token_type == YAML_FLOW_ENTRY_TOKEN)) {
				DC_PRINTF("Parsing value from a key list: '%s'\n", token.data.scalar.value);
				/* Convert string name of keycode to its decimal value */
				struct keycode_t *keycode = get_keycode((char *)token.data.scalar.value);
				if (keycode == 0)
					handle_error(UNKNOWN_KEYCODE, current_binding->name, (char *)token.data.scalar.value);
				uint16_t dec_value = keycode->value;
				/* Add it to the binding->keys array */
				current_binding->keycount++;
				current_binding->keys = reallocarray(current_binding->keys, current_binding->keycount, sizeof(uint16_t));
				current_binding->keys[current_binding->keycount - 1] = dec_value;	
			} else if (parser_state == ACTION_KEY && last_token_type == YAML_VALUE_TOKEN) {
				DC_PRINTF("Parsing action's value: '%s'\n", token.data.scalar.value);
				parser_state = ACTION_VALUE;
				/* Copy action value to binding */
				alloc_strcpy(&current_binding->action, (char *)token.data.scalar.value);
				parsed_action = true;
			} else if (parser_state == AUTOREPEAT_KEY && last_token_type == YAML_VALUE_TOKEN) {
				DC_PRINTF("Parsing autorepeat value: '%s'\n", token.data.scalar.value);
				parser_state = AUTOREPEAT_VALUE;
				current_binding->autorepeat = value_to_bool((char *)token.data.scalar.value);
			}
			break ;
		default:
			DC_PRINTF("Unknown token type (%i)\n", token.type);
			break;
		}
		last_token_type = token.type;
		if (token.type != YAML_STREAM_END_TOKEN)
			yaml_token_delete(&token);
	} while (token.type != YAML_STREAM_END_TOKEN);
	DC_PRINTF("Config was parsed successfully, found %li bindings\n", binding_count);

	if (binding_count < 1)
		exit_err("No bindings found in config", EXIT_WITHOUT_ERRNO);
	
	// Deinitialize parser
	yaml_parser_delete(&parser);
}

