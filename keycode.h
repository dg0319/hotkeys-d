/* Keycode name-value file handling
 * These function and structs are used for reading keycode file, parsing, storing,
 * and converting string keycode values to their numerical representation */

#ifndef KEYCODE_H
#define KEYCODE_H

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <linux/limits.h>

#define KEY_NAME_MAX 32
#define KEY_VALUE_MAX 9

#define BINDING_NAME_MAX 256

struct binding_t {
	char *name;
	uint16_t *keys;
	size_t keycount;
	char *action;
	bool autorepeat;
};

struct keycode_t {
	char name[KEY_NAME_MAX];
	uint16_t value;
};

extern char keycode_path[PATH_MAX];
extern struct keycode_t *keycodes;
extern size_t keycode_count;

/* Returns keycode_t struct for given key name */
struct keycode_t *get_keycode(char *key_name);

/* Parse *data array into keycode_t array and set *parsed_count_ptr to
 * its length */
void parse_keycodes(char *data, long datasz);

/* Load keycode file, parse it, fill *keycodes array and set keycode_count
 * to its size */
void load_keycodes(void);

#endif

