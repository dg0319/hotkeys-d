#!/bin/sh
# Generate 'keycodes' file from existing keycodes in input-event-codes.h(or from file, specified as arg)
header_path="/usr/include/linux/input-event-codes.h"
if [ -n "$1" ]; then
	if [ ! -x "$1" ]; then
		echo "$1 doesn't exist"
		exit 1
	fi
fi
grep -e '^#define KEY' "$header_path" | tr -s ' ' '\t' | cut -f 2,3 | tee keycodes
