/* YAML hotkey config parser 
 * These functions are used to load YAML hotkey config file and parse it*/

#ifndef CONFIG_H
#define CONFIG_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <err.h>
#include <error.h>
#include <yaml.h>
#include "keycode.h"

extern char config_path[PATH_MAX];
extern struct binding_t *bindings;
extern size_t binding_count;

/* Load YAML config from file, parse it, fill binding array
   *bindings and set *binding_count_ptr to the parsed binding count */
void load_config(char * path);

#endif

