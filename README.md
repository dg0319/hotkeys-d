Description
====================
It is a simple hotkey daemon, which allows to bind shell commands for a key combination(or a single key).  
  
It is not fully completed yet, but the main functionality described below works.

Requirements
====================
libyaml

Configuration
====================
Two files are used for configuration: __./keycodes__ and __./hotkeys.yaml__.
  
__keycodes__ is used to store name and value(it can be decimal, HEX or another previously defined keycode's name) of every key. 

__hotkeys.yaml__ is used to store hotkeys configuration in such format:
  
```yml
binding_name:
	keys: [KEY_1, KEY_2]
	action: 'shell_commands'
```

An example:
  
```yml
start_ff:
        keys: [KEY_LEFTCTRL, KEY_5]
        action: 'firefox &'

kb_leds_on:
        keys: [KEY_KBDILLUMUP]
        action: 'echo 1 > /sys/class/leds/asus::kbd_backlight/brightness'
```

_example_name_ is a name of a hotkey binding.
  
_action_ is a shell command to be executed when the hotkey binding is triggered.
  
_keys_ is the YAML array with key names (must have 1+ elements) from the __keycodes__ file.
  
_autorepeat_ is an optional boolean field, which enables repeated shell _action_ execution if the hotkey is hold. 
  
By default (if binding has no _autorepeat_ field set to _true_) _action_ will be executed only when the key was pressed (command will be executed only once, and won't be executed repeatedly, if the hotkey is hold).
  

__keycodes__ format:

```
KEY_NAME	KEY_VALUE
```

This file can be generated (if there is a need) from __input-event-codes.h__ using

```sh
./genkeycodes.sh [path to input-event-codes.h]
```


Both config files can be updated while the daemon is running (a bit buggy at the moment).

Usage
====================
See 
```sh
./hotkeys-d --help
```
