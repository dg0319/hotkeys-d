/* These functions are used to monitor modifications in configuration files
 * using inotify */

#ifndef INOTIFY_UTIL_H
#define INOTIFY_UTIL_H

/* This function will be called when any modification of config or 
 * keycode file is detected */
extern void reload_settings(char *changed_file);

void init_inotify();
void close_inotify();

/* Add config and keycode files to be monitored for modification */
void watch_files(char *config_path, char *keycode_path);

#endif

