#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/input.h>
#include <stdint.h>
#include <unistd.h>
#include <stdbool.h>
#include "keydev.h"
#include "util.h"

#ifdef DEBUG_KEYDEV
	#define DD_PRINTF printf
	#define DD_PRINTFS printf("[Main] ");printf
	#define DD_PUTS puts
	#define DD_PUTSS printf("[Main] ");puts
	#define DD_PUTCHAR putchar
#else
	#define DD_PRINTF(...)
	#define DD_PRINTFS(...)
	#define DD_PUTS(...)
	#define DD_PUTSS(...)
	#define DD_PUTCHAR(...)
#endif

extern void print_event_mask(uint64_t mask);
	
/* Filter function for scandir */
static int filter_evt(const struct dirent *ent)
{
	return (strncmp(ent->d_name, "event", 5) == 0);
}

/* Check if event mask belongs to the key-like device */
static __always_inline bool check_syn_key_evt(uint64_t evmask)
{
	return ((evmask & (1 << EV_KEY)) && (evmask & (1 << EV_SYN)));
}

size_t get_key_devices(struct keydevice_t **devices)
{
	struct dirent **namelist;
	size_t entry_count = scandir(event_dir, &namelist, filter_evt, 0);
	struct keydevice_t *keydev = (struct keydevice_t*)calloc(entry_count, sizeof(struct keydevice_t));
	size_t keydev_count = 0;
	DD_PRINTF("%s has %li dirents\n", event_dir, entry_count);
	for (size_t i = 0; i < entry_count; i++) {
		int fd = -1;
		char path_buf[PATH_MAX + NAME_MAX];
		strcpy(path_buf, event_dir);
		strcat(path_buf, namelist[i]->d_name);
		if ((fd = open(path_buf, O_RDONLY)) < 0) {
			DD_PRINTF("Device path: %s, fd: %i\n", path_buf, fd);
			exit_err("Failed to open %s", EXIT_WITH_ERRNO, path_buf); 
		}
		DD_PRINTF("%s:\n", path_buf);

		/* Read device name */
		char devname_buf[128];
		if (ioctl(fd, EVIOCGNAME(sizeof(devname_buf)), devname_buf) < 0)
			exit_err("ioctl to read device name for %s", EXIT_WITH_ERRNO, path_buf); 
		DD_PRINTF("EVIOCGNAME: %s\n", devname_buf);

		/* Get event mask of a device */
		uint64_t evmask;
		if (ioctl(fd, EVIOCGBIT(0, EV_MAX), &evmask) < 0)
			exit_err("ioctl to get event mask for %s", EXIT_WITH_ERRNO, path_buf); 
		DD_PRINTF("Event mask: %li\n", evmask);

		#ifdef DEBUG_MAIN
			/* Print event types of a device */
			DD_PUTS("Supported event types:");
			print_event_mask(evmask);
			DD_PUTCHAR('\n');
		#endif

		/* Check if this device is key-like device */
		if (check_syn_key_evt(evmask)) {
			keydev[keydev_count].fd = fd;
			strcpy(keydev[keydev_count].path, path_buf);
			strcpy(keydev[keydev_count].name, devname_buf);
			keydev_count++;
			DD_PUTS("It is a key-like device\n");	
		}

		if (devices == 0) {
			printf("%s\t%s\t%li", path_buf, devname_buf, evmask);
			print_event_mask(evmask);
			putchar('\n');
		}	
	}
	if (devices == 0)
		exit(EXIT_SUCCESS);
	*devices = keydev;
	DD_PRINTFS("Total key-like device count: %li\n", keydev_count);
	return keydev_count;
}
