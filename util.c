#include "util.h"
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <syslog.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>
#include "log.h"
#include "tty_util.h"

void exit_err(char *str, bool print_errno, ...)
{
	va_list args;
	va_start(args, print_errno);

	char msgbuf[512];
	vsnprintf(msgbuf, sizeof(msgbuf), str, args);
	va_end(args);

	if (!detached)
		enable_echo();

	if (print_errno)
		log_msg(LOG_MSG_ERROR, "%s: %s", msgbuf, strerror(errno));
	else
		log_msg(LOG_MSG_ERROR, "%s", msgbuf);

	log_msg(LOG_MSG_ERROR, "Fatal error, exiting");

	exit(EXIT_FAILURE);
}

void daemonize(void)
{
	/* Detach from the tty */
	int tty_fd;
	if ((tty_fd = open("/dev/tty", O_RDWR)) < 0)
		exit_err("Failed to open /dev/tty for detaching", true);
	if (ioctl(tty_fd, TIOCNOTTY) == -1)
		exit_err("Failed to detach", true);
	detached = true;

	if (daemon(1, 1) != 0)
		exit_err("Failed to daemonize", true);
}

void fread_to_array(const char* path, char **data, long *data_len)
{
	FILE *f;
	if ((f = fopen(path, "r")) == 0)
		exit_err("Failed to open file %s for reading", EXIT_WITH_ERRNO, path);
	
	// Get file size
	fseek(f, 0, SEEK_END);
	*data_len = ftell(f);
	rewind(f);

	*data = (char *)calloc(*data_len, sizeof(char));
	
	fread((void*)*data, *data_len, 1, f);
	if (ferror(f) != 0)
		exit_err("An error has happened while reading %s", EXIT_WITH_ERRNO, path);

	if (fclose(f) != 0)
		exit_err("An error has happened while closing %s", EXIT_WITH_ERRNO, path);
}
