#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <linux/input.h>
#include <stdint.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/epoll.h>
#include <getopt.h>
#include <signal.h>
#include "keydev.h"
#include "keycode.h"
#include "config.h"
#include "log.h"
#include "util.h"
#include "inotify_util.h"
#include "tty_util.h"

#ifdef DEBUG_MAIN
	#define DM_PRINTF printf
	#define DM_PRINTFS printf("[Main] ");printf
	#define DM_PUTS puts
	#define DM_PUTSS printf("[Main] ");puts
	#define DM_PUTCHAR putchar
#else
	#define DM_PRINTF(...)
	#define DM_PRINTFS(...)
	#define DM_PUTS(...)
	#define DM_PUTSS(...)
	#define DM_PUTCHAR(...)
#endif

#define KEYRELEASE_EVENT 0
#define KEYPRESS_EVENT 1
#define AUTOREPEAT_EVENT 2

const char *event_dir = "/dev/input/";
char config_path[PATH_MAX] = "hotkeys.yaml";
char keycode_path[PATH_MAX] = "keycodes";
bool detached = false;

struct keycode_t *keycodes = 0;
size_t keycode_count = 0;
static bool *pressed_keys;
struct binding_t *bindings = 0;
size_t binding_count = 0;
static char *program_name; 
static bool show_keycodes_mode = false;
static void (*keypress_handler) (unsigned int keycode, uint8_t event_type);

void print_event_mask(uint64_t mask)
{
	if (mask & (1 << EV_SYN))
		printf("EV_SYN ");
	if (mask & (1 << EV_KEY))
		printf("EV_KEY ");
	if (mask & (1 << EV_REL))
		printf("EV_REL ");
	if (mask & (1 << EV_ABS))
		printf("EV_ABS ");
	if (mask & (1 << EV_MSC))
		printf("EV_MSC ");
	if (mask & (1 << EV_SW))
		printf("EV_SW ");
	if (mask & (1 << EV_LED))
		printf("EV_LED ");
	if (mask & (1 << EV_SND))
		printf("EV_SND ");
	if (mask & (1 << EV_REP))
		printf("EV_REP ");
	if (mask & (1 << EV_FF))
		printf("EV_FF ");
	if (mask & (1 << EV_PWR))
		printf("EV_PWR ");
	if (mask & (1 << EV_FF_STATUS))
		printf("EV_FF_STATUS ");
}


/* Print out the keycode's name (specified in 'keycodes' file) */
static void print_keycode(unsigned int keycode, uint8_t event_type)
{
	/* Find the name of a numeric keycode */
	char *name = 0;
	for (size_t i = 0; i < keycode_count; i++)
		if (keycodes[i].value == keycode) {
			name = keycodes[i].name;
			break;
		}

	printf("Handling ");
	if (event_type == KEYPRESS_EVENT) {
		 printf("key press ");
	} else if (event_type == KEYRELEASE_EVENT) {
		 printf("key release ");
	} else if (event_type == AUTOREPEAT_EVENT) {
		 printf("autorepeat ");
	}

	if (name == 0) {
		printf("for unknown key (%i)\n", keycode);
	} else {
		printf("for %s (%i)\n", name, keycode);
	} 
}

static inline void execute_action(struct binding_t *binding, uint8_t event_type)
{
	log_msg(LOG_MSG_INFO, "Executing shell action for hotkey (%s) '%s'\n", (event_type == AUTOREPEAT_EVENT ? "autorepeat" : "keypress"), binding->name);
	int rs = system(binding->action); /* execute found action as a shell command */
	log_msg(LOG_MSG_INFO, "Execution of %s ended with return code '%i'\n", binding->name, rs);
}

static void handle_keypress(unsigned int keycode, uint8_t event_type)
{
	#ifdef DEBUG_MAIN
	/* Print name of a pressed keycode  */
	print_keycode(keycode, event_type);
	/* Print pressed keys' keycodes */
	if (event_type == KEYPRESS_EVENT) {
		DM_PRINTFS("Pressed keys: ");
	} else if (event_type == AUTOREPEAT_EVENT) {
		DM_PRINTFS("Autorepeated keys: ");
	}
	for (size_t i = 0; i < keycode_count; i++)
		if (pressed_keys[i] == true)
		       DM_PRINTF("%li ", i);
	DM_PUTCHAR('\n');
	#endif

	/* Get pressed keys count */
	size_t pressed_count = 0;
	for (size_t i = 0; i < keycode_count; i++)
		if (pressed_keys[i] == true)
			pressed_count++;

	#ifdef DEBUG_MAIN
	if (event_type == KEYPRESS_EVENT) {
		DM_PRINTFS("Pressed key count: %li\n", pressed_count);
	} else if (event_type == AUTOREPEAT_EVENT) {
		DM_PRINTFS("Autorepeated key count: %li\n", pressed_count);
	}
	for (size_t i = 0; i < binding_count; i++) {
		DM_PRINTFS("Keycount for '%s': %li\n", bindings[i].name, bindings[i].keycount);
	}
	#endif

	/* Iterate through all configured bindings and compare their keycodes with pressed */
	for (size_t i = 0; i < binding_count; i++) {
		if (bindings[i].keycount == pressed_count) {
			size_t equalcodes = 0;
			/* search key from binding in the pressed key array */
			for (size_t j = 0; j < bindings[i].keycount; j++) {
				if (pressed_keys[bindings[i].keys[j]] == true)
					equalcodes++;
			}
			/* execute hotkey's shell action, if all keycodes are equal */
			if (equalcodes == bindings[i].keycount) {
				if ((event_type == KEYPRESS_EVENT) || (bindings[i].autorepeat
						       	&& event_type == AUTOREPEAT_EVENT))
					execute_action(&(bindings[i]), event_type);
			}
		}
	}
}

static void read_input_event(int fd)
{
	char buf[sizeof(struct input_event)];
	ssize_t rd;
	/* Read input_event struct from fd */
       	if ((rd = read(fd, (void*)buf, sizeof(buf)) < 0))
			exit_err("Failed to read input event data from fd %i\n", EXIT_WITH_ERRNO, fd);
	struct input_event *evt = (struct input_event*)buf;
	DM_PUTSS("Received input event");
	DM_PRINTFS("\tTimestamp(usec): %ld\n", evt->time.tv_usec);
	DM_PRINTFS("\tKeycode: %i\n", evt->code);
	DM_PRINTFS("\tType: %i\n", evt->type);
	DM_PRINTFS("\tValue: %i", evt->value);
	/* Check if it is a key event */
	if (evt->type & EV_KEY) {
		if (evt->value == KEYRELEASE_EVENT) {
			DM_PUTS(" (released) ");
			pressed_keys[evt->code] = false;
		}
		else if (evt->value == KEYPRESS_EVENT || evt->value == AUTOREPEAT_EVENT) {
			DM_PRINTFS(" (%s) ", (evt->value == KEYPRESS_EVENT ? "pressed" : "autorepeated"));
			pressed_keys[evt->code] = true;
			keypress_handler(evt->code, evt->value);
		}
	}
	DM_PUTCHAR('\n');
}

static void show_usage(void)
{
	printf("Usage: %s [OPTION] [-k KEYCODES_FILE] [-c CONFIG_FILE]\n", program_name);
}

static void show_version(void)
{
	puts("hotkey-d v0.0");
}

static void show_help(void)
{
	show_usage();
	putchar('\n');
	printf("\
  --help                     Show this help\n\
  --version                  Output version info\n\
  --usage                    Output usage info\n\
  -s, --show-keycodes        Do not do anything, just show keycodes of pressed keys\n\
  -l, --list-devices         List available input devices and their names\n\
  -k, --keycodes-file=FILE   Do not execute any shell actions, just show \n\
                             pressed keycodes' values\n\
  -c, --config-file=FILE     Use FILE as a configuration file\n");
}

/* Will be called when when any change in config files is detected */ 
void reload_settings(char *changed_file)
{
	log_msg(LOG_MSG_WARNING, "%s was changed, reloading", changed_file);

	/* Reload keycodes file */
	free(keycodes);
	keycodes = 0;
	keycode_count = 0;
	load_keycodes();

	/* Reload config file */
	if (!show_keycodes_mode) {
		free(bindings);
		bindings = 0;
		binding_count = 0;
		load_config(config_path);
	}
}

static void handle_signal(int sig, siginfo_t *info, void *ucontext)
{
	log_msg(LOG_MSG_INFO, "Signal %i received, exiting...", sig);
	close_inotify();
	if (!detached)
		enable_echo();
	exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[])
{
	/* Parse command-line options */
	program_name = argv[0];
	const struct option long_options[] = {
		{"help", no_argument, 0, 1},
		{"version", no_argument, 0, 2},
		{"usage", no_argument, 0, 3},
		{"list-devices", no_argument, 0, 'l'},
		{"show-keycodes", no_argument, 0, 's'},
		{"config-file", required_argument, 0, 'c'},
		{"keycodes-file", required_argument, 0, 'k'}
	};
	int option_index = 0;
	int r;
	while ((r = getopt_long(argc, argv, "sc:k:l", long_options, &option_index)) != -1) {
		switch (r) {
		case 's':
			log_msg(LOG_MSG_INFO, "Turned on keycode show mode");
			show_keycodes_mode = true;
			break;
		case 'c':
			if (show_keycodes_mode) {
				log_msg(LOG_MSG_INFO,
					       	"Will not use any config due to keycode show mode");
				break;
			}
			log_msg(LOG_MSG_INFO, "Explicitly set config file %s will be used\n", optarg);
			if (strlen(optarg) + 1 > PATH_MAX) {
				exit_err("Config file path exceeds file path limit (%i)\n", EXIT_WITHOUT_ERRNO, PATH_MAX);
			}
			strcpy(config_path, optarg);
			break;
		case 'k':
			log_msg(LOG_MSG_INFO, "Explicitly set keycodes file %s will be used\n", optarg);
			if (strlen(optarg) + 1 > PATH_MAX) {
				exit_err("Keycodes file path exceeds file path limit (%i)\n", EXIT_WITHOUT_ERRNO, PATH_MAX);
			}
			strcpy(keycode_path, optarg);
			break;
		case 1:
			show_help();
			exit(EXIT_SUCCESS);
			break;
		case 2:
			show_version();
			exit(EXIT_SUCCESS);
			break;
		case 'l':
			get_key_devices(0);
			exit(EXIT_SUCCESS);
			break;
		case 3:
			show_usage(); 
			exit(EXIT_SUCCESS);
			break;
		case '?': /* Case of an invalid option */
			show_help();
			exit(EXIT_SUCCESS);
			break;
		}
	}
	
	/* Read keycode names and values from keycode file */
	load_keycodes();

	struct sigaction act;
	act.sa_sigaction = handle_signal;
	act.sa_flags = SA_SIGINFO;
	sigaction(SIGINT, &act, 0); 
	sigaction(SIGTERM, &act, 0); 

	if (show_keycodes_mode) {
		disable_echo();
		keypress_handler = &print_keycode;
	} else {
		load_config(config_path);
		keypress_handler = &handle_keypress;
		daemonize();
		log_msg(LOG_MSG_INFO, "Daemon has started");
	}
	
	init_inotify();
	watch_files(config_path, keycode_path);

	/* Initialize array for capturing keypresses */
	pressed_keys = (bool*)calloc(keycode_count, sizeof(bool));

	/* Get all key-like /dev/input/event* devices */
	struct keydevice_t *devices;
	size_t devcount = get_key_devices(&devices);

	/* Create epoll instance for monitoring /dev/input/event* fds  */
	int efd;
	if ((efd = epoll_create(devcount)) < 0)
		exit_err("epoll create failed", EXIT_WITH_ERRNO);
	struct epoll_event *events = (struct epoll_event*)calloc(devcount, sizeof(struct epoll_event));
	
	/* Iterate through found key-like devices, add their FDs for epoll monitoring (wait for read) */
	for (size_t i = 0; i < devcount; i++) {
		events[i].events = EPOLLIN;
		events[i].data.fd = devices[i].fd; /* fd of a monitored device file */
		DM_PRINTFS("Adding %s [%i] (%s)  for epoll monitoring\n", devices[i].path, devices[i].fd, devices[i].name);
		int cr;
		if ((cr = epoll_ctl(efd, EPOLL_CTL_ADD, events[i].data.fd, events + i) < 0)) /* Add this fd to epoll instance */
			exit_err("Failed to add device %s [%s] fd to epoll", EXIT_WITH_ERRNO, devices[i].path, devices[i].name);
	}
	
	/* Array of received epoll events */
	struct epoll_event *r_events = (struct epoll_event*)calloc(devcount, sizeof(struct epoll_event));
	while (1) {
		int rfds; /* Number of ready fds */
		/* Wait for any incoming data on added fds */
		if ((rfds = epoll_wait(efd, r_events, devcount, -1)) == 0)
			exit_err("epoll_wait", EXIT_WITH_ERRNO); 
		/* Iterate through received epoll events and handle reading for their fds */
		for (size_t i = 0; i < rfds; i++) {
			int fd = r_events[i].data.fd;
			read_input_event(fd);
		}
	}

	exit(EXIT_SUCCESS);
}

