#ifndef KEYDEV_H
#define KEYDEV_H

/* These functions are used to retrieve key-like input devices */

#include <linux/limits.h>
#include <stdlib.h>

struct keydevice_t {
	char path[PATH_MAX]; /* path to the device (like /dev/input/event*) */
	char name[128]; /* device description */
	int fd;
};

extern const char *event_dir;

/* Retrieve all input devices' paths and their description, 
   store them in *devices array, and return size of that array
   If **devices is 0, will only print path and description
   for every key-like device and quit */
size_t get_key_devices(struct keydevice_t **devices);

#endif

