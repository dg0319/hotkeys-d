/* Message logging functions */

#ifndef LOG_H
#define LOG_H

enum log_msg_type {
	LOG_MSG_INFO,
	LOG_MSG_WARNING,
       	LOG_MSG_ERROR
};

/* Print message to the stdout/stderr (if not running as a daemon), and
 * send it to the syslog */
void log_msg(enum log_msg_type type, char *str, ...);

#endif

