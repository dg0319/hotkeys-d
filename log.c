#include <stdio.h>
#include <syslog.h>
#include <stdbool.h>
#include <stdarg.h>
#include <string.h>
#include "log.h"

extern bool detached;

void log_msg(enum log_msg_type type, char *str, ...)
{
	va_list args;
	va_start(args, str);

	char buf[1024];
	vsnprintf(buf, sizeof(buf), str, args);

	if (!detached) {
		FILE *stream = (type == LOG_MSG_ERROR ? stderr : stdout);
		fputs(buf, stream);
		if (buf[strlen(buf) - 1] != '\0')
			fputc('\n', stream);
	}

	if (type == LOG_MSG_INFO)
		syslog(LOG_DAEMON | LOG_INFO, buf);
	else if (type == LOG_MSG_WARNING)
		syslog(LOG_DAEMON | LOG_WARNING, buf);
	else if (type == LOG_MSG_ERROR)
		syslog(LOG_DAEMON | LOG_ERR, buf);

	va_end(args);
}

