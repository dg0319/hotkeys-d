/* These functions are used to manage controlling terminal */

#ifndef TTY_UTIL_H
#define TTY_UTIL_H

#include <stdbool.h>

extern bool detached;

void disable_echo(void);

void enable_echo(void);

#endif

