#include "inotify_util.h"
#include <sys/inotify.h>
#include <sys/epoll.h>
#include <pthread.h>
#include <unistd.h>
#include <libgen.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include "log.h"
#include "util.h"

static int inotify_fd;
static int config_wd;
static int keycode_wd;
static char *config_basename;
static char *keycode_basename;

static uint32_t wd_event_mask = IN_MODIFY | IN_CLOSE_WRITE | IN_MOVE_SELF 
| IN_DELETE_SELF;

void init_inotify()
{
	if ((inotify_fd = inotify_init()) == -1)
		exit_err("Failed to init inotify instance", EXIT_WITH_ERRNO);
}

void close_inotify()
{
	if ((inotify_fd = close(inotify_fd)) < 0)
		log_msg(LOG_MSG_ERROR, "Failed to close inotify fd");
}

static void read_inotify_event()
{
	struct inotify_event event;
	if (read(inotify_fd, (void*)&event, sizeof(struct inotify_event)) == -1) {
			log_msg(LOG_MSG_ERROR, "Failed to read inotify event");
			return;
	}

	if (event.mask | wd_event_mask) {
		sleep(1);
		if (event.wd == config_wd) {
			reload_settings(config_basename);
		} else if (event.wd == keycode_wd) {
			reload_settings(keycode_basename);
		}
	}
}

static void *start_watch_thread(void *arg)
{
	int epoll_fd;
	if ((epoll_fd = epoll_create(2)) < 0)
		exit_err("Failed to create epoll instance", EXIT_WITH_ERRNO);

	/* Add inotify fd to the epoll instance */
	struct epoll_event epoll_events;
	epoll_events.events = EPOLLIN;
	epoll_events.data.fd = inotify_fd;
	if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, inotify_fd, &epoll_events) != 0)
		exit_err("Failed to add inotify fd to epoll", EXIT_WITH_ERRNO);

	/* Wait for and receive inotify_event structs from inotify_fd */
	while (1) {
		int rfd = epoll_wait(epoll_fd, &epoll_events, 1, 600);
		if (rfd == 1)
			read_inotify_event();
		else if (rfd == -1) {
			if (errno == EINTR)
				/* Will retry epoll_wait anyway if got 
				 * interrupted system call error */
				errno = 0;
			else if (errno != 0)
				log_msg(LOG_MSG_ERROR, "epoll_wait for inotify fd failed: %s",
						strerror(errno));
		}
	}

	return 0;
}

void watch_files(char *config_path, char *keycode_path)
{
	config_basename = basename(config_path);
	keycode_basename = basename(keycode_path);

	if ((config_wd = inotify_add_watch(inotify_fd, config_path, wd_event_mask)) == -1)
		exit_err("Failed to add watch for config (%s)", EXIT_WITH_ERRNO, 
				config_path);

	if ((keycode_wd = inotify_add_watch(inotify_fd, keycode_path, wd_event_mask)) == -1)
		exit_err("Failed to add watch for keycode file (%s)", 
				EXIT_WITH_ERRNO, config_path);

	pthread_t config_watch_thread;
	if (pthread_create(&config_watch_thread, 0, start_watch_thread, 0) != 0)
		exit_err("Failed to start inotify watching threading", 
				EXIT_WITH_ERRNO);
}

